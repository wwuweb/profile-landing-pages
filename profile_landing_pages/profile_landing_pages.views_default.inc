<?php
/**
 * @file
 * profile_landing_pages.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function profile_landing_pages_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'profiles';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Profiles';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'huxley-current-students profiles-2x2';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '4';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'responsive_grid';
  $handler->display->display_options['style_options']['columns'] = '2';
  $handler->display->display_options['style_options']['wrapper_classes'] = 'views-responsive-gird';
  $handler->display->display_options['style_options']['default_classes'] = 1;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  /* Field: Content: Full size photo */
  $handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
  $handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
  $handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
  $handler->display->display_options['fields']['field_photo']['label'] = '';
  $handler->display->display_options['fields']['field_photo']['element_type'] = '0';
  $handler->display->display_options['fields']['field_photo']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_photo']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_photo']['element_wrapper_class'] = 'profile-2x2-image';
  $handler->display->display_options['fields']['field_photo']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_photo']['settings'] = array(
    'image_style' => '130w_x_170h',
    'image_link' => '',
  );
  /* Field: Content: Alumni Photo */
  $handler->display->display_options['fields']['field_alumni_photo']['id'] = 'field_alumni_photo';
  $handler->display->display_options['fields']['field_alumni_photo']['table'] = 'field_data_field_alumni_photo';
  $handler->display->display_options['fields']['field_alumni_photo']['field'] = 'field_alumni_photo';
  $handler->display->display_options['fields']['field_alumni_photo']['label'] = '';
  $handler->display->display_options['fields']['field_alumni_photo']['element_type'] = '0';
  $handler->display->display_options['fields']['field_alumni_photo']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_alumni_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_alumni_photo']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_alumni_photo']['element_wrapper_class'] = 'profile-2x2-image';
  $handler->display->display_options['fields']['field_alumni_photo']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_alumni_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_alumni_photo']['settings'] = array(
    'image_style' => '130w_x_170h',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['title']['element_wrapper_class'] = 'profile-2x2-name';
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Graduating Class */
  $handler->display->display_options['fields']['field_graduating_class']['id'] = 'field_graduating_class';
  $handler->display->display_options['fields']['field_graduating_class']['table'] = 'field_data_field_graduating_class';
  $handler->display->display_options['fields']['field_graduating_class']['field'] = 'field_graduating_class';
  $handler->display->display_options['fields']['field_graduating_class']['label'] = '';
  $handler->display->display_options['fields']['field_graduating_class']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_graduating_class']['alter']['text'] = 'Class of [field_graduating_class]';
  $handler->display->display_options['fields']['field_graduating_class']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_graduating_class']['element_label_type'] = '0';
  $handler->display->display_options['fields']['field_graduating_class']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_graduating_class']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_graduating_class']['element_wrapper_class'] = 'profile-2x2-class';
  $handler->display->display_options['fields']['field_graduating_class']['settings'] = array(
    'format_type' => 'year',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['body']['element_type'] = '0';
  $handler->display->display_options['fields']['body']['element_label_type'] = '0';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['body']['element_wrapper_class'] = 'profile-2x2-bio';
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '250',
  );
  /* Field: Content: Link */
  $handler->display->display_options['fields']['view_node']['id'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['view_node']['field'] = 'view_node';
  $handler->display->display_options['fields']['view_node']['label'] = '';
  $handler->display->display_options['fields']['view_node']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['view_node']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['view_node']['element_wrapper_class'] = 'profile-2x2-link';
  $handler->display->display_options['fields']['view_node']['text'] = 'READ MORE';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'profile' => 'profile',
  );

  /* Display: Meet Current Students */
  $handler = $view->new_display('panel_pane', 'Meet Current Students', 'panel_pane_1');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'profiles-current-students profiles-2x2';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'profile' => 'profile',
  );
  $handler->display->display_options['pane_title'] = 'Meet Current Students';

  /* Display: Meet Alumnus */
  $handler = $view->new_display('panel_pane', 'Meet Alumnus', 'panel_pane_2');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'profiles-alumnus profiles-2x2';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'alumni_profiles' => 'alumni_profiles',
  );
  $handler->display->display_options['pane_title'] = 'Meet Alumnus';
  $export['profiles'] = $view;

  return $export;
}
