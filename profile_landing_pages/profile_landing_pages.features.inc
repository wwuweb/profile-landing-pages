<?php
/**
 * @file
 * profile_landing_pages.features.inc
 */

/**
 * Implements hook_views_api().
 */
function profile_landing_pages_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function profile_landing_pages_image_default_styles() {
  $styles = array();

  // Exported image style: 130w_x_170h.
  $styles['130w_x_170h'] = array(
    'name' => '130w_x_170h',
    'label' => '140w x 180h',
    'effects' => array(
      7 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 140,
          'height' => 180,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
